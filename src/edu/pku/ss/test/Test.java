package edu.pku.ss.test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;

public class Test {

	// 预处理的文档总数.
	@SuppressWarnings("unused")
	private static final int totoalDocNum = 129292;

	public static void main(String[] args) {
	}

	public static void createWordMatrix() {
		try {
			List<String> tfidfLines = FileUtils
					.readLines(new File("word_tfidf"));

			// 1.第一步，获取行对应的词频词典.
			Map<Integer, Map<String, Double>> lineWordTFIDFMap = new HashMap<Integer, Map<String, Double>>();

			int currentLine = 0;
			for (String tfidfLine : tfidfLines) {

				Map<String, Double> wordTDIDFMap = new HashMap<String, Double>();
				String[] items = tfidfLine.split(" ");
				String word;
				double tdidf;
				List<String> itemList = Arrays.asList(items);
				Iterator<String> iter = itemList.iterator();
				while (iter.hasNext()) {
					word = iter.next();
					tdidf = Double.parseDouble(iter.next());
					wordTDIDFMap.put(word, tdidf);
				}

				lineWordTFIDFMap.put(currentLine, wordTDIDFMap);

				System.out.println("第" + (currentLine + 1) + "行处理完成!");

				++currentLine;
			}

			// 2.读取词项词典.
			List<String> vocabulary = FileUtils.readLines(new File(
					"all_devep_test_word_vocabulary"));

			System.out.println("字典读取完成");

			// 3.构造.
			Map<String, double[]> wordLineFreqMap = new HashMap<String, double[]>();
			int i = 0;
			double[] row = null;

			for (String word : vocabulary) {
				row = new double[129292];
				Set<Integer> keySet = lineWordTFIDFMap.keySet();
				int j = 0;
				for (Integer doc : keySet) {
					if (lineWordTFIDFMap.get(doc).containsKey(word)) {
						row[j] = lineWordTFIDFMap.get(doc).get(word);
					} else {
						row[j] = 0;
					}
					++j;
				}

				wordLineFreqMap.put(word, row);
				System.out.println("正在处理第" + (i + 1) + "单词");
				++i;
			}

			// 4.写入文件.
			int line = 0;
			Map<String, Integer> wordFreqMatrixIndex = new HashMap<String, Integer>();
			for (String word : wordLineFreqMap.keySet()) {
				wordFreqMatrixIndex.put(word, line);
				++line;
				double[] docFreqMap = wordLineFreqMap.get(word);

				FileUtils.write(new File("term_line_tfidf_matrix"),
						stringFromArray(docFreqMap), true);
				System.out.println("正在寫入第" + line + "行!");
			}
			writeMapToFile(wordFreqMatrixIndex, new File("tfidf_index"));
			System.out.println("索引保存至文件完成!");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static <V> void writeMapToFile(Map<String, V> mapToWrite, File file) {
		try {
			List<String> wordFreqLines = new ArrayList<String>();
			for (String key : mapToWrite.keySet()) {
				wordFreqLines.add(key + "\t" + mapToWrite.get(key));
			}
			FileUtils.writeLines(file, wordFreqLines);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static String stringFromArray(double[] items) {
		StringBuilder sb = new StringBuilder();

		int size = items.length;
		int i;
		for (i = 0; i < size - 1; ++i) {
			sb.append(items[i]).append(",");
		}
		sb.append(items[i]);
		sb.append("\n");

		return sb.toString();
	}

	public static String stringFromArray(int[] items) {
		StringBuilder sb = new StringBuilder();

		int size = items.length;
		int i;
		for (i = 0; i < size - 1; ++i) {
			sb.append(items[i]).append("\t");
		}
		sb.append(items[i]);
		sb.append("\n");

		return sb.toString();
	}

	/**
	 * 统计文档集合中包含某一个单词的文档数量.即产生一个文件，文件中每一个单词和对应的包含该单词的文档数量。
	 */
	public static void createWordSetNum() {
		try {

			// 读取词表,构建hashmap
			List<String> words = FileUtils.readLines(new File(
					"all_devep_test_word_vocabulary"));
			Map<String, Integer> wordDocsMap = new HashMap<String, Integer>();
			for (String word : words) {
				wordDocsMap.put(word, 0);
			}

			// 读取索引，用来确定索引对应的单词
			List<String> indexes = FileUtils.readLines(new File("freq_index"));
			String[] wordIndex = null;
			Map<Integer, String> indexWordMap = new HashMap<Integer, String>();
			for (String index : indexes) {
				wordIndex = index.split("\t");
				indexWordMap.put(Integer.parseInt(wordIndex[1]), wordIndex[0]);
			}

			List<String> lines = FileUtils.readLines(new File(
					"term_line_freq_matrix"));
			for (int i = 0; i < 5685; ++i) {
				String line = lines.get(i);
				String[] items = line.split("\t");
				String key = indexWordMap.get(i);
				for (int j = 0; j < 129292; ++j) {
					if (Integer.parseInt(items[j]) != 0) {
						wordDocsMap.put(key, wordDocsMap.get(key) + 1);
					}
				}
				System.out.println("已经处理第" + i + "行矩阵！");
			}

			// 写入文件
			writeMapToFile(wordDocsMap, new File("word_lines"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 计算每一篇文档中在词表中的单词的tfidf值。
	 */
	public static void createWordVocabTFIDF() {
		try {
			List<String> vocab = FileUtils.readLines(new File(
					"all_devep_test_word_vocabulary"));

			// 构建map，确定每一个单词出现的文档数量。
			Map<String, Integer> wordLinesMap = new HashMap<String, Integer>();
			List<String> wordNumLines = FileUtils.readLines(new File(
					"word_lines"));
			String[] two = null;
			for (String line : wordNumLines) {
				two = line.split("\t");
				wordLinesMap.put(two[0], Integer.parseInt(two[1]));
			}

			List<String> lines = FileUtils.readLines(new File("filtered_tf"));
			int count = 0;

			List<String> linesToWrite = new ArrayList<String>();

			for (String line : lines) {
				count++;
				Map<String, Double> wordTFIDFMap = new HashMap<String, Double>();

				String[] items = line.split(" ");
				List<String> itemList = Arrays.asList(items);
				Iterator<String> iter = itemList.iterator();
				String word;
				double tf;
				double tfidf;
				double idf;
				while (iter.hasNext()) {
					word = iter.next();
					tf = Double.parseDouble(iter.next());
					if (vocab.contains(word)) {
						idf = Math
								.log(129292 * 1.0 / (wordLinesMap.get(word) + 1))
								/ Math.log(2);
						tfidf = tf * idf;
						wordTFIDFMap.put(word, tfidf);
					}
				}

				linesToWrite.add(stringFromMap(wordTFIDFMap));
				System.out.println("处理第" + count + "篇文档完成！");
			}

			FileUtils.writeLines(new File("word_tfidf"), linesToWrite);
			System.out.println("已经将所有单词的tfidf值写入了文件!");

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static <T> String stringFromMap(Map<String, T> map) {
		StringBuilder sb = new StringBuilder();
		for (String key : map.keySet()) {
			sb.append(key).append(" ").append(map.get(key)).append(" ");
		}
		return sb.toString();
	}
}
