package edu.pku.ss.test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

import org.apache.commons.io.FileUtils;

import edu.pku.ss.engine.EngineOption;
import edu.pku.ss.evaluation.Evaluator;
import edu.pku.ss.evaluation.EvaluatorHelper;
import edu.pku.ss.evaluation.Problem;
import edu.pku.ss.helper.PathHelper;

public class TestRelease {

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		// 1.读取问题.
		List<Problem> problems = EvaluatorHelper.readProblems(
				PathHelper.Test.FILE_TEST_PROBLEM,
				Charset.forName("UTF-8"));

		// 2.使用投票方法，获取每到选项，得到一个字符串表示的列表.也可以是用单一方法测试
		//例如：EvaluatorHelper.autoClozeBySingle(EngineOption.WORD_2_VEC).
		List<String> results = EvaluatorHelper.autoClozeBySingle(problems, EngineOption.WORD_2_VEC);
		
		// 3.将计算结果写入文件
		FileUtils.writeLines(new File("test_result_1"), results);

		// 4.计算准确率.
		double accuracyRate = Evaluator.getInstance()
				.standardEvaluatePrecision(results);
		// 5.打印准确率.
		System.out.println(accuracyRate);
	}
}
