package edu.pku.ss.clean;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;

import edu.pku.ss.helper.PathHelper;

/**
 * 该类执行最基础的数据清洗工作，该清洗工作主要包含以下四个步骤： 1）去除每一篇文档中的版权信息等等；
 * 2）扩充语料，对语料语料中的每一个文档进行拆分。如果某一文档中还有多个chapter，就拆分为多个文档，否则不进行任何拆分。
 * 3）将句子组合到一行中去；在文档中有许多句子被断行了，即分布在一个句子被分在了多行，这样是影响分词和词形标注等
 * 准确率的，所以需要将分布在多行的句子，组合到一行中去。 4）去除文档中无用的空行。在文档中有许多连续的空行，所要将其进行去除。
 * 
 * 注意：经过以上四步之后，新的语料中，每一个文档内部，每一个行就是一个段落，不再以空行标识段落。
 * 
 * @author 王志伟
 * 
 */
public class Cleaner {

	/**
	 * 原始训练数据所在的目录。
	 */
	private static final String trainingDataDir = "Training_Data";

	/**
	 * 清洗版权信息后的数据放在的目录。每一个文档的名字不变.
	 */
	private static final String copytightDir = "preprocess/clean/copy_right_data";

	/**
	 * 以章节为单位扩充原语料。这一步骤主要考虑的是将相同主题的内容聚合在一个文本中，也就是说一个文本仅仅代表一个主题.
	 */
	private static final String chapterDir = "chapter_data";

	/**
	 * 在语料中会发现，很多句子被断行了，这样的语料会影响分词和词性标注的。所以需要将断行的句子恢复到一行之中.
	 */
	private static final String sentenceDir = "preprocess/clean/sentenced_data";

	/**
	 * 在语料中有许多文档含有多行无用的空行，需要去除空行，将去除后的结果写出新的目录下面。
	 */
	private static final String noBlankDir = "no_blank_data";

	/**
	 * 执行数据清洗。
	 */
	public static void clean() {
		step1();
		step2();
		step3();
		step4();
	}

	private static void step1() {
		try {
			File dataDir = new File(trainingDataDir);
			for (File f : dataDir.listFiles()) {
				doStep1(FileUtils.readLines(f), f.getName());
				Logger.getAnonymousLogger().info(f.getName() + " completes!");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void step2() {
		try {
			File dataDir = new File(copytightDir);
			for (File f : dataDir.listFiles()) {
				doStep2(FileUtils.readLines(f), f.getName());
				Logger.getAnonymousLogger().info(f.getName() + " completes!");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// //////////////////////////////////////////////////////
	public static void step3() {
		try {
			File dataDir = new File(copytightDir);
			for (File f : dataDir.listFiles()) {
				doStep3(FileUtils.readLines(f), f.getName());
				Logger.getAnonymousLogger().info(f.getName() + " completes!");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void step4() {
		try {
			File dataDir = new File(sentenceDir + "_522");
			for (File f : dataDir.listFiles()) {
				doStep4(FileUtils.readLines(f), f.getName());
				Logger.getAnonymousLogger().info(f.getName() + " completes!");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// //////////////////////////////////////////////////////////////////////////
	/**
	 * 过滤掉太小的文件，为了语料的平衡.
	 */
	public static void step5() {
		try {
			File dir = new File(PathHelper.Clean.DIR_SENTENCED_NGRAM_DATA);
			int i = 0;
			for (File file : dir.listFiles()) {
				List<String> lines = FileUtils.readLines(file);
				int count = 0;
				String[] words;
				for (String line : lines) {
					words = line.split("\t");
					count += words.length;
					if (count > 20) {
						FileUtils.copyFile(file,
								new File("tmp/" + file.getName()));
						break;
					}
				}
				i++;
				System.out.println("第" + i + "个文件扫描完毕!");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// /////////////////////////////////////////////////////////////////////////

	/**
	 * 此函数是一个临时函数，主要用于对给定数据集的观察。 1、观察看是否每一个文档都具有章节。
	 * 
	 * @param dir
	 * @param encoding
	 */
	public static void dataLookat1(String dir, Charset encoding) {
		try {
			File dataDir = new File(dir);
			List<String> lines = null;
			/**
			 * 统计每一个文档含有多少"Chapter".
			 */
			int[] dataChapNum = new int[dataDir.listFiles().length];
			Arrays.fill(dataChapNum, 0);
			int i = 0;
			for (File f : dataDir.listFiles()) {
				System.out.println(f.getName());
				lines = FileUtils.readLines(f, encoding);
				for (String line : lines) {
					if (line.toLowerCase().startsWith("chapter")) {
						dataChapNum[i] += 1;
					}
				}
				++i;
			}

			for (int j = 0; j < dataChapNum.length; ++j) {
				System.out.println(dataChapNum[j]);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 第一步数据清洗：过滤掉每一篇文档中的版权等说明信息.
	 * 
	 * @param lines
	 *            以行表示的源文件.
	 */
	private static void doStep1(final List<String> lines, String filename) {
		try {
			int size = lines.size();
			for (int i = 0; i < size; ++i) {
				if (lines.get(i).startsWith("*END*THE")) {
					FileUtils.writeLines(
							new File(copytightDir + "/" + filename),
							lines.subList(i + 1, size));
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 重新构建语料。如果一篇文档中含有多个chapter的话，那么将这些chapter拆分出来，当作一个单独的文本，如果该篇
	 * 文档不含有chapter，那么这个文档不做任何处理。处理之后，。写入新的文件。
	 * 
	 * @param lines
	 * @return
	 */
	private static void doStep2(final List<String> lines, String filename) {
		List<Integer> chapterIndexes = new ArrayList<Integer>();

		/**
		 * 1、检索出文本中的章节行索引。
		 */
		for (int i = 0; i < lines.size(); ++i) {
			if (lines.get(i).startsWith("CHAPTER")) {
				chapterIndexes.add(i);
			}
		}

		/**
		 * 2、对文本进行分割和写入新的目录下面。
		 */
		try {
			if (chapterIndexes.isEmpty()) {
				FileUtils.writeLines(new File(chapterDir + "/" + filename),
						lines);
			} else {
				for (int i = 0; i < chapterIndexes.size() - 1; ++i) {
					FileUtils.writeLines(new File(chapterDir + "/" + filename
							+ "_" + i), lines.subList(chapterIndexes.get(i)
							.intValue(), chapterIndexes.get(i + 1).intValue()));
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 第三步数据清洗：文档中的句子被断行了，所以需要将被断行的句子恢复在一个行。
	 * 
	 * @param lines
	 */
	private static String doStep3(final List<String> lines, String filename) {
		if (lines == null || lines.isEmpty()) {
			return new String();
		}

		StringBuilder combinedText = new StringBuilder();
		String last = lines.get(0);
		boolean isLastEmpty = false;
		if (last.isEmpty()) {
			isLastEmpty = true;
			combinedText.append("\n");
		} else {
			combinedText.append(last);
		}

		String current = null;
		for (int i = 1; i < lines.size(); ++i) {
			current = lines.get(i);
			if (current.isEmpty()) {
				isLastEmpty = true;
			} else {
				isLastEmpty = false;
			}

			if (!isLastEmpty && !current.isEmpty()) {
				combinedText.append(" " + current);
			} else {
				combinedText.append("\n");
			}
		}

		try {
			FileUtils.write(new File(sentenceDir + "_522" + "/" + filename),
					combinedText);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return combinedText.toString();
	}

	/**
	 * 数据清洗第4步：清除语料中每一个文档中的空行。
	 * 
	 * @param lines
	 * @param filename
	 */
	private static void doStep4(final List<String> lines, String filename) {
		List<String> noBlankLines = new ArrayList<String>();
		for (String line : lines) {
			if (!line.isEmpty()) {
				noBlankLines.add(line);
			}
		}

		try {
			FileUtils.writeLines(
					new File(noBlankDir + "_522" + "/" + filename),
					noBlankLines);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
