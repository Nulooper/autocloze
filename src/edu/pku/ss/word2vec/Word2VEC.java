package edu.pku.ss.word2vec;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;

import edu.pku.ss.helper.MathHelper;
import edu.pku.ss.helper.PathHelper;

public class Word2VEC {

	private List<String> vocabulary;
	private int wordNum;
	private int wordVecLength;
	private Map<String, double[]> wordVecMap;

	private Logger logger = Logger.getLogger("edu.pku.ss.word2vec.Word2VEC");

	public Word2VEC() {
		this.wordNum = 5685;
		this.wordVecLength = 20;
		this.initVocabulary();
		this.initWordVecMap();
	}

	public double getSimilarity(String term1, String term2) {
		if (term1 == null || term2 == null || term1.isEmpty()
				|| term2.isEmpty()) {
			logger.warning(term1 + "或者" + term2 + "不合法，为null或者空!返回默认相似度值0.");
			return 0;
		}
		if (term1.equals(term2)) {
			return 1.0;
		}
		return MathHelper.cosine(this.wordVecMap.get(term1),
				this.wordVecMap.get(term2));
	}

	public double getSumSimilarity(String term1, List<String> terms) {
		if (term1 == null || terms == null || term1.isEmpty()
				|| terms.isEmpty()) {
			logger.warning(term1 + "或者" + terms + "不合法，为null或者空!返回默认相似度值0.");
			return 0;
		}

		double sum = 0.0;
		for (String term : terms) {
			sum += getSimilarity(term1, term);
		}
		return sum;
	}

	public List<String> getVocabulary() {
		return this.vocabulary;
	}

	public int getWordNum() {
		return this.wordNum;
	}

	public int getWordVecLength() {
		return this.wordVecLength;
	}

	private void initVocabulary() {
		try {
			vocabulary = FileUtils.readLines(new File(
					PathHelper.Vocabulary.VOCABULARY));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void initWordVecMap() {
		try {
			this.wordVecMap = new HashMap<String, double[]>();
			List<String> lines = FileUtils.readLines(new File(
					PathHelper.EngineModel.WORD_2_VEC_RAW));
			int vecStart = 1;
			int vecSize = lines.size();
			for (int i = vecStart; i < vecSize; ++i) {
				String[] items = lines.get(i).split(" ");
				String word = items[0];
				if (!this.vocabulary.contains(word)) {
					continue;
				}
				double[] vec = new double[200];
				for (int j = 1; j < 200; ++j) {
					vec[j] = Double.parseDouble(items[j]);
				}
				this.wordVecMap.put(word, vec);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
