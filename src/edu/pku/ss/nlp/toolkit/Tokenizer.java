package edu.pku.ss.nlp.toolkit;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.PTBTokenizer;

public class Tokenizer {
	private static Tokenizer tokenizer = new Tokenizer();

	private Tokenizer() {
	}

	public static Tokenizer getInstance() {
		return tokenizer;
	}

	public String[] tokenize(String input) {
		// If the input is null or empty, return empty array.
		if (input == null || input.isEmpty()) {
			return new String[0];
		}

		PTBTokenizer<CoreLabel> ptbt = new PTBTokenizer<CoreLabel>(
				new StringReader(input), new CoreLabelTokenFactory(), "");
		List<String> tokens = new ArrayList<String>();
		for (CoreLabel label; ptbt.hasNext();) {
			label = (CoreLabel) ptbt.next();
			tokens.add(label.toString());
		}
		String[] returnTokens = new String[tokens.size()];
		tokens.toArray(returnTokens);
		return returnTokens;
	}
}
