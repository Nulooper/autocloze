package edu.pku.ss.nlp.toolkit;

import java.io.FileInputStream;
import java.io.InputStream;

import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;

/**
 * 该类用来句子分割。
 * 
 * @author 王志伟
 * @version 0.0.1
 */
public class SentenceSplitter {

	private static SentenceSplitter instance = new SentenceSplitter();

	private static final String sentModelPath = "resource/models/opennlp/en-sent.bin";

	private SentenceDetectorME sentenceDetector;

	private SentenceSplitter() {
		try {
			InputStream modelIn = new FileInputStream(sentModelPath);
			SentenceModel model = new SentenceModel(modelIn);
			this.sentenceDetector = new SentenceDetectorME(model);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public static SentenceSplitter getInstance() {
		return instance;
	}

	public String[] segment(String input) {
		if (input == null || input.isEmpty()) {
			return new String[0];
		}
		return this.sentenceDetector.sentDetect(input);
	}

}
