package edu.pku.ss.ngram;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;

import edu.pku.ss.evaluation.EvaluatorHelper;
import edu.pku.ss.evaluation.Problem;
import edu.pku.ss.helper.IOHelper;
import edu.pku.ss.nlp.toolkit.SentenceSplitter;
import edu.pku.ss.nlp.toolkit.Tokenizer;

/**
 * 该类将语料中每一篇文档编程单词向量组表示。每一行是一个单词数组，代表一个句子。
 * 
 * @author 王志伟
 * 
 */
public class NgramHelper {

	private static final String inputDirPath = "no_blank_data";
	private static final String outputDirPath = "preprocess/word_verctor_sentence_doc";

	/**
	 * 将训练语料中的所有文档表示成单词向量。在文档中，每一行是一个单词数组，该单词数组表示一个句子。
	 */
	public static void createWordVectorForDocs() {
		try {
			File inputDir = new File(inputDirPath);
			int i = 0;
			for (File file : inputDir.listFiles()) {
				i++;
				List<String> lines = FileUtils.readLines(file);
				String[] sentences = null;
				List<String> sentenceList = new ArrayList<String>();
				for (String line : lines) {
					sentences = SentenceSplitter.getInstance().segment(line);
					String[] words = null;
					for (String sentence : sentences) {
						words = Tokenizer.getInstance().tokenize(sentence);
						sentenceList.add(arrayToString(words));
					}
				}
				FileUtils.writeLines(
						new File(outputDirPath + "/" + file.getName()),
						sentenceList);
				System.out.println("第" + i + "个文件处理完成!");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param sentence
	 *            单词数组表示的句子.
	 * @return
	 */
	private static String arrayToString(String[] sentence) {
		if (sentence == null || sentence.length == 0) {
			return "\n";
		}
		StringBuilder sb = new StringBuilder();
		for (String word : sentence) {
			sb.append(word).append("\t");
		}
		sb.append("\n");
		return sb.toString();
	}

	/**
	 * 统计开发集合和测试题目中的二元组.
	 */
	public static void createBigramsForDevelopAndTest() {
		try {
			Set<String> bigramSet = new HashSet<String>();

			List<Problem> developProblems = EvaluatorHelper.readProblems(
					"development_set.txt", Charset.forName("UTF-8"));
			bigramSet.addAll(getBigramSet(developProblems));

			List<Problem> testProblems = EvaluatorHelper.readProblems(
					"test_set.txt", Charset.forName("UTF-8"));
			bigramSet.addAll(getBigramSet(testProblems));

			FileUtils.writeLines(new File(
					"preprocess/new_develop_test_ngram/bigram_freq/bigram"),
					new ArrayList<String>(bigramSet), true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 获取二元组.如果有两个单词word1和word2，那么二元的格式为"word1_word2".
	 * 
	 * @param problems
	 * @return
	 */
	private static Set<String> getBigramSet(List<Problem> problems) {
		Set<String> bigramSet = new HashSet<String>();
		int count = 0;
		for (Problem p : problems) {
			count++;

			for (String bigram : p.bigrams) {
				bigramSet.add(bigram);
			}

			System.out.println("正在处理第" + count + "个问题");
		}
		return bigramSet;
	}

	/**
	 * 获取三元组.如果有三个单词word1,word2和word3，那么三元的格式为"word1_word2_word3".
	 * 
	 * @param problems
	 * @return
	 */
	private static Set<String> getTrigramSet(List<Problem> problems) {
		Set<String> trigramSet = new HashSet<String>();
		int count = 0;
		for (Problem p : problems) {
			count++;
			int size = p.rawProblemWords.size();
			for (int i = 0; i < size - 2; ++i) {
				trigramSet.add(p.rawProblemWords.get(i) + "_"
						+ p.rawProblemWords.get(i + 1) + "_"
						+ p.rawProblemWords.get(i + 2));
			}
			System.out.println("正在处理第" + count + "个问题");
		}
		return trigramSet;
	}

	/**
	 * 统计开发集合和测试集合中的三元组.
	 */
	public static void createTrigramsForDevelopAndTest() {
		try {
			Set<String> trigramSet = new HashSet<String>();
			List<Problem> developProblems = EvaluatorHelper.readProblems(
					"development_set.txt", Charset.forName("UTF-8"));
			trigramSet.addAll(getTrigramSet(developProblems));
			List<Problem> testProblems = EvaluatorHelper.readProblems(
					"test_set.txt", Charset.forName("UTF-8"));
			trigramSet.addAll(getTrigramSet(testProblems));

			FileUtils.writeLines(new File(
					"preprocess/develop_test_ngram/trigram_freq/trigram"),
					new ArrayList<String>(trigramSet), true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 统计每一篇语料中的二元组和三元组，并且记录他们的频次。
	 * 然后分别写入目录set_doc_bigram_freq和set_doc_trigram_freq。
	 */
	public static void createDocGramFreqMap() {
		try {
			File dir = new File(
					"preprocess/new_word_verctor_sentence_doc_for_ngram");
			Map<String, Integer> bigramFreqMap = null;
			Map<String, Integer> trigramFreqMap = null;
			int count = 0;
			for (File f : dir.listFiles()) {
				count++;
				List<List<String>> sentenceList = new ArrayList<List<String>>();

				List<String> lines = FileUtils.readLines(f);
				String[] words = null;
				for (String line : lines) {
					words = line.split("\t");
					sentenceList.add(Arrays.asList(words));
				}

				bigramFreqMap = getBigramFreqMap(sentenceList);
				trigramFreqMap = getTrigramFreqMap(sentenceList);

				IOHelper.writeMapToFile(bigramFreqMap, new File(
						"preprocess/new_set_doc_bigram_freq/" + f.getName()));
				IOHelper.writeMapToFile(trigramFreqMap, new File(
						"preprocess/new_set_doc_trigram_freq/" + f.getName()));

				System.out.println("第" + count + "个文件处理完成！");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static Map<String, Integer> getBigramFreqMap(
			List<List<String>> sentences) {
		Map<String, Integer> bigramFreqMap = new HashMap<String, Integer>();
		String bigram = null;
		for (List<String> sentence : sentences) {
			int size = sentence.size();
			for (int i = 0; i < size - 1; ++i) {
				bigram = sentence.get(i) + "_" + sentence.get(i + 1);
				if (bigramFreqMap.containsKey(bigram)) {
					bigramFreqMap.put(bigram, bigramFreqMap.get(bigram) + 1);
				} else {
					bigramFreqMap.put(bigram, 1);
				}
			}
		}
		return bigramFreqMap;
	}

	private static Map<String, Integer> getTrigramFreqMap(
			List<List<String>> sentences) {
		Map<String, Integer> trigramFreqMap = new HashMap<String, Integer>();
		String trigram = null;
		for (List<String> sentence : sentences) {
			int size = sentence.size();
			for (int i = 0; i < size - 2; ++i) {
				trigram = sentence.get(i) + "_" + sentence.get(i + 1) + "_"
						+ sentence.get(i + 2);
				if (trigramFreqMap.containsKey(trigram)) {
					trigramFreqMap
							.put(trigram, trigramFreqMap.get(trigram) + 1);
				} else {
					trigramFreqMap.put(trigram, 1);
				}
			}
		}
		return trigramFreqMap;
	}
}
