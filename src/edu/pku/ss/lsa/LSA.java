package edu.pku.ss.lsa;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;

import edu.pku.ss.helper.MathHelper;
import edu.pku.ss.helper.MatrixHelper;

public class LSA {

	private static final Logger logger = Logger.getLogger("edu.pku.ss.lsa.LSA");

	private String matrixPath;
	private String wordMatrixIndex;
	private int rowDimension;
	private int columnDemension;

	/**
	 * wait to initialize.
	 */
	private double[][] termMatrix = null;
	private Map<String, Integer> termIndexMap = new HashMap<String, Integer>();

	public LSA(String matrixPath, String wordMatrixIndex, int row, int column) {
		this.matrixPath = matrixPath;
		this.wordMatrixIndex = wordMatrixIndex;
		this.rowDimension = row;
		this.columnDemension = column;

		this.initTermIndexMap();
		this.initTermMatrix();
	}

	public static LSA createLSA(String matrixPath, String wordMatrixIndex,
			int row, int column) {
		return new LSA(matrixPath, wordMatrixIndex, row, column);
	}

	/**
	 * 计算两个词项之间的相似度值。
	 * 
	 * @param term1
	 * @param term2
	 * @return
	 */
	public double getSimilarity(String term1, String term2) {
		if (term1 == null || term2 == null || term1.isEmpty()
				|| term2.isEmpty()) {
			return 0.0;// 此处的0值有待商榷，因为实际的相似度值有可能是负值。
		}
		if (!termIndexMap.containsKey(term1)
				|| !termIndexMap.containsKey(term2)) {
			logger.warning("矩阵中不包含:\t" + term1 + "\t" + term2);
			return 0.0;
		}
		int index1 = this.termIndexMap.get(term1.toLowerCase());
		int index2 = this.termIndexMap.get(term2.toLowerCase());
		double[] vec1 = this.termMatrix[index1];
		double[] vec2 = this.termMatrix[index2];
		return MathHelper.cosine(vec1, vec2);
	}

	/**
	 * 计算term1与terms列表中所有单词的相似度之和.
	 * 
	 * @param term1
	 * @param terms
	 * @return
	 */
	public double getSumSimilarity(String term1, List<String> terms) {
		if (term1 == null || term1.isEmpty() || terms == null
				|| terms.isEmpty()) {
			return 0.0;// wait to test.
		}
		double sum = 0.0;
		for (String term : terms) {
			sum += getSimilarity(term1, term);
		}
		return sum;
	}

	/**
	 * 计算term1与terms数组中所有单词的相似度之和.
	 * 
	 * @param term1
	 * @param terms
	 * @return
	 */
	public double getSumSimilarity(String term1, String[] terms) {
		List<String> termList = Arrays.asList(terms);
		return this.getMeanSimilary(term1, termList);
	}

	public double getBigramSimilarity(String term1, List<String> bigrams) {
		double sumSimilarity = 0.0;
		double similarity = 0.0;
		for (String bigram : bigrams) {
			similarity = getSimilarity(term1, bigram);
			sumSimilarity += similarity;
		}
		return sumSimilarity;
	}

	public double getBigramSumSimilarity(String term1, String[] bigrams) {
		return getBigramSimilarity(term1, Arrays.asList(bigrams));
	}

	/**
	 * 计算term1和terms列表中每一个term的相似度平均值。
	 * 
	 * @param term1
	 * @param terms
	 * @return
	 */
	public double getMeanSimilarity(String term1, String[] terms) {
		List<String> termList = Arrays.asList(terms);
		return this.getMeanSimilary(term1, termList);
	}

	/**
	 * 计算term1和terms列表中每一个term的相似度平均值。
	 * 
	 * @param term1
	 * @param terms
	 * @return
	 */
	public double getMeanSimilary(String term1, List<String> terms) {
		if (term1 == null || terms == null || term1.isEmpty()
				|| terms.size() == 0) {
			return 0.0;// 此处的0值有待商榷，因为实际的相似度值有可能是负值。
		}
		double simiSum = 0.0;
		for (String term : terms) {
			simiSum += this.getSimilarity(term1, term);
		}

		return simiSum / terms.size();
	}

	public Map<String, Double> getMeanSimilarity(List<String> selections,
			List<String> terms) {

		Map<String, Double> answersMap = new HashMap<String, Double>();
		for (String selection : selections) {
			answersMap.put(selection, getMeanSimilary(selection, terms));
		}

		return answersMap;
	}

	private void initTermIndexMap() {
		try {
			List<String> lines = FileUtils.readLines(new File(wordMatrixIndex));
			for (String line : lines) {
				String[] items = line.split("\t");
				this.termIndexMap.put(items[0], Integer.parseInt(items[1]));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void initTermMatrix() {
		this.termMatrix = MatrixHelper.readMatrix(this.matrixPath,
				rowDimension, columnDemension, Charset.forName("UTF-8"));
	}
}
