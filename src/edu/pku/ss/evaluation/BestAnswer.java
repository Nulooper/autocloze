package edu.pku.ss.evaluation;

public class BestAnswer extends AbstractAnswer {

	public Selection selection;

	public BestAnswer(String id, Selection selection) {
		super(id);
		this.selection = selection;
	}

	@Override
	public String toString() {
		return id + ")" + " " + this.selection.toString();
	}
}
