package edu.pku.ss.evaluation;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;

import edu.pku.ss.engine.AutoCloze;
import edu.pku.ss.engine.EngineOption;
import edu.pku.ss.nlp.toolkit.Tokenizer;

public class EvaluatorHelper {

	public static List<Problem> readProblems(String filename, Charset encoding)
			throws IOException {
		File file = new File(filename);
		List<Problem> problems = new ArrayList<Problem>();
		List<String> lines = FileUtils.readLines(file);
		Iterator<String> iter = lines.iterator();
		String problem = null;
		String a = null;
		String b = null;
		String c = null;
		String d = null;
		String e = null;
		while (iter.hasNext()) {

			List<String> problemWords = new ArrayList<String>();
			List<Selection> selections = new ArrayList<Selection>();
			String id;

			problem = iter.next();
			a = iter.next();
			b = iter.next();
			c = iter.next();
			d = iter.next();
			e = iter.next();
			iter.next();
			iter.next();

			// 获取题号和题目中的单词.
			int idIndex = problem.indexOf(")");
			id = problem.substring(0, idIndex);
			String[] items = Tokenizer.getInstance().tokenize(
					problem.substring(idIndex + 2));
			for (String item : items) {
				problemWords.add(item.toLowerCase());
			}

			// 获取该题目的选项.
			selections.add(getSelection(a.toLowerCase()));
			selections.add(getSelection(b.toLowerCase()));
			selections.add(getSelection(c.toLowerCase()));
			selections.add(getSelection(d.toLowerCase()));
			selections.add(getSelection(e.toLowerCase()));

			problems.add(new Problem(problemWords, selections, id));
		}
		return problems;
	}

	private static Selection getSelection(String item) {
		int index = item.indexOf(" ");
		String id = item.substring(index - 2, index - 1);
		String word = item.substring(index + 1);
		return new Selection(id, word);
	}

	public static List<String> autoClozeByVote(List<Problem> problems) {
		List<BestAnswer> answers = new ArrayList<BestAnswer>();

		for (Problem problem : problems) {
			String id = problem.id;
			Selection best = AutoCloze.getInstance().getBestByVote(problem);
			answers.add(new BestAnswer(id, best));
		}

		List<String> answersStrs = new ArrayList<String>();
		for (BestAnswer a : answers) {
			answersStrs.add(a.toString());
		}
		return answersStrs;
	}

	public static List<String> autoClozeBySingle(List<Problem> problems,
			EngineOption option) {
		List<BestAnswer> answers = new ArrayList<BestAnswer>();

		for (Problem problem : problems) {
			String id = problem.id;
			Selection best = AutoCloze.getInstance().getBestBySingle(option,
					problem);
			answers.add(new BestAnswer(id, best));
		}

		List<String> answersStrs = new ArrayList<String>();
		for (BestAnswer a : answers) {
			answersStrs.add(a.toString());
		}
		return answersStrs;
	}

}
