package edu.pku.ss.evaluation;

import java.util.List;

public class SortedAnswer extends AbstractAnswer {

	public List<Selection> sortedSelections;

	public SortedAnswer(String id, List<Selection> sortedSelections) {
		super(id);
		this.sortedSelections = sortedSelections;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(id + ") (");
		for (Selection s : sortedSelections) {
			sb.append(s.id + ", ");
		}
		sb.replace(sb.length() - 2, sb.length(), ")");
		return sb.toString();
	}
}
