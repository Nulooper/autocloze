package edu.pku.ss.evaluation;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;

import edu.pku.ss.helper.PathHelper;

public class Problem {

	private static final Logger logger = Logger
			.getLogger("edu.pku.ss.evaluation.Problem");

	public static List<String> stopwordList;
	static {
		try {
			stopwordList = FileUtils.readLines(new File(
					PathHelper.StopWord.STOP_WORDS));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static final String BLANK = "_____";

	/**
	 * 每一个元素是一个单词的列表, 包含"_____".note:測試通過.
	 */
	public List<String> rawProblemWords;
	/**
	 * 问题句子中的单词列表，去除"_____"之后的列表.note:測試通過.
	 */
	public List<String> validWords;
	/**
	 * "_____"左边的单词列表.note:測試通過.
	 */
	public List<String> leftWords;
	/**
	 * "_____"右边的单词列表.note：測試通過.
	 */
	public List<String> rightWords;

	public List<String> filteredLeftWords;
	public List<String> filteredRightWords;
	public List<String> filteredWords;

	/**
	 * 这个句子中的bigrams列表.包含leftBigrams和rightBigrams.
	 */
	public List<String> bigrams;
	/**
	 * "_____"左边的bigram列表.
	 */
	public List<String> leftBigrams;
	/**
	 * "_____"右边的bigram列表.
	 */
	public List<String> rightBigrams;

	/**
	 * 所有的单词，包括题干和选项中的单词.
	 */
	public List<String> totalWords;

	public List<Selection> selections;

	public String id;

	public Map<Selection, List<String>> candidateSentsMap;

	public Problem(List<String> rawProblemWords, List<Selection> selections,
			String id) {
		this.rawProblemWords = rawProblemWords;
		this.selections = selections;
		this.id = id;

		this.initWords();
		this.initBigrams();
		this.initCandidateSents();
	}

	private void initWords() {
		this.validWords = new ArrayList<String>();
		this.leftWords = new ArrayList<String>();
		this.rightWords = new ArrayList<String>();

		this.filteredWords = new ArrayList<String>();
		this.filteredLeftWords = new ArrayList<String>();
		this.filteredRightWords = new ArrayList<String>();

		int index = this.rawProblemWords.indexOf(BLANK);
		if (index == -1) {
			logger.warning("问题" + this.id + "读取错误，没有读到: " + BLANK);
			return;
		}

		String leftWord = null;
		for (int i = 0; i < index; ++i) {
			leftWord = this.rawProblemWords.get(i);
			this.validWords.add(leftWord);
			this.leftWords.add(leftWord);

			if (!stopwordList.contains(leftWord)) {
				this.filteredLeftWords.add(leftWord);
				this.filteredWords.add(leftWord);
			}
		}

		int size = this.rawProblemWords.size();
		String rightWord = null;
		for (int j = index + 1; j < size; ++j) {
			rightWord = this.rawProblemWords.get(j);
			this.validWords.add(rightWord);
			this.rightWords.add(rightWord);

			if (!stopwordList.contains(rightWord)) {
				this.filteredRightWords.add(rightWord);
				this.filteredWords.add(rightWord);
			}
		}

		// 初始化totalWords.
		this.totalWords = new ArrayList<String>();
		for (Selection s : this.selections) {
			this.totalWords.add(s.word);
		}
		this.totalWords.addAll(this.validWords);
	}

	private void initBigrams() {
		this.bigrams = new ArrayList<String>();

		this.leftBigrams = new ArrayList<String>();
		this.createBigrams(leftWords);

		this.rightBigrams = new ArrayList<String>();
		this.createBigrams(rightWords);

		this.bigrams.addAll(leftBigrams);
		this.bigrams.addAll(rightBigrams);
	}

	private void createBigrams(List<String> words) {
		int size = words.size();
		if (size <= 1) {
			logger.warning("单词列表长度小于2，不进行二元组检索，直接返回！");
			return;
		}
		for (int i = 0; i < size - 1; ++i) {
			this.leftBigrams.add(words.get(i) + "_" + words.get(i + 1));
		}
	}

	public void initCandidateSents() {
		this.candidateSentsMap = new HashMap<Selection, List<String>>();
		int index = this.rawProblemWords.indexOf(BLANK);
		for (Selection s : this.selections) {
			this.rawProblemWords.remove(index);
			this.rawProblemWords.add(index, s.word);
			List<String> sent = new ArrayList<String>(this.rawProblemWords);
			candidateSentsMap.put(s, sent);
		}
	}

	public List<String> getSentence(Selection s) {
		return this.candidateSentsMap.get(s);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.id + "\t");
		for (String word : this.rawProblemWords) {
			sb.append(word).append(" ");
		}
		sb.append("\n");
		for (Selection s : this.selections) {
			sb.append(s.toString()).append("\n");
		}
		return sb.toString();
	}
}
