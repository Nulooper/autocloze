package edu.pku.ss.evaluation;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;

import edu.pku.ss.helper.PathHelper;

public class Evaluator {

	private static Evaluator instance = new Evaluator();

	private List<String> standardAnswers;

	private Evaluator() {
		this.initStandardAnswers();
	}

	public static Evaluator getInstance() {
		return instance;
	}

	/**
	 * 
	 * @param combinedAnswers
	 *            使用组合答案格式："801) [d]"
	 * @return
	 */
	public double combineEvaluatePrecision(List<String> combinedAnswers) {
		if (combinedAnswers == null || combinedAnswers.isEmpty()
				|| combinedAnswers.size() != getStandardAnswers().size()) {
			return 0.0;
		}
		int length = getStandardAnswers().size();
		int rights = 0;
		String comparedSubStr = null;
		String standardAnswer = null;
		for (int i = 0; i < length; ++i) {
			standardAnswer = getStandardAnswers().get(i);
			comparedSubStr = standardAnswer.substring(0,
					standardAnswer.indexOf("]") + 1);
			if (combinedAnswers.get(i).equals(comparedSubStr)) {
				rights++;
			}
		}
		return rights * 1.0 / length * 100;
	}

	/**
	 * 注：測試通過程序沒有錯誤！
	 * 
	 * @param yourAnswers
	 *            使用标准格式："802) [b] clean"
	 * @return
	 */
	public double standardEvaluatePrecision(List<String> yourAnswers) {
		if (yourAnswers == null || yourAnswers.isEmpty()
				|| yourAnswers.size() != getStandardAnswers().size()) {
			return 0.0;
		}
		int length = getStandardAnswers().size();
		int rights = 0;

		List<String> rightAnswers = new ArrayList<String>();

		for (int i = 0; i < length; ++i) {
			if (yourAnswers.get(i).equals(getStandardAnswers().get(i))) {
				rights++;
				rightAnswers.add(yourAnswers.get(i));
			}
		}
		try {
			FileUtils.writeLines(new File("right_answer"), rightAnswers);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return rights * 1.0 / length * 100;
	}

	private void initStandardAnswers() {
		try {
			File file = new File(
					PathHelper.Development.FILE_DEVELOP_PROBLEM_ANSWERS);
			this.setStandardAnswers(FileUtils.readLines(file));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public List<String> getStandardAnswers() {
		return standardAnswers;
	}

	public void setStandardAnswers(List<String> standardAnswers) {
		this.standardAnswers = standardAnswers;
	}
}
