package edu.pku.ss.advance;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;

public class ParagraphSplitter {

	/**
	 * 段落计数，同时也是段落文档的个数.
	 */
	private static int count = 0;

	private static final Logger logger = Logger
			.getLogger("edu.pku.ss.advance.ParagraphSplitter");

	public static void splitParagraphs(String inputDir, String outputDir,
			int fold) {
		try {
			int i = 0;
			File dir = new File(inputDir);
			List<String> lines = null;
			for (File file : dir.listFiles()) {
				lines = FileUtils.readLines(file);
				doSplitParagraphs(lines, outputDir, fold);

				logger.info("第" + (++i) + "文档处理结束!");
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	private static void doSplitParagraphs(List<String> lines, String outputDir,
			int fold) {
		try {
			List<String> paragraphs = new ArrayList<String>();
			Iterator<String> iter = lines.iterator();
			StringBuilder sb = new StringBuilder();
			String line = null;
			while (iter.hasNext()) {
				line = iter.next();
				if (line.isEmpty()) {
					if (!"".equals(sb.toString())) {
						paragraphs.add(sb.toString());
					}
					sb = new StringBuilder();
				} else {
					sb.append(line).append(" ");
				}
			}
			paragraphs.add(sb.toString());

			for (String para : paragraphs) {
				FileUtils.write(new File(outputDir + "/" + fold + "_"
						+ (++count)), para);
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
}
