package edu.pku.ss.advance;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;

public class CorpusTransformer {
	public static void createCorpusForWord2Vec() {
		try {
			File sentencedDir = new File(
					"E:\\BaiduYun\\Workspace\\AutoCloze\\AutoCloze\\preprocess\\clean\\sentenced_ngram_data");
			int i = 0;
			for (File file : sentencedDir.listFiles()) {
				StringBuilder sb = new StringBuilder();
				List<String> lines = FileUtils.readLines(file);
				for (String line : lines) {
					if (line.isEmpty()) {
						continue;
					}
					sb.append(line.replaceAll("\t", " ")).append("\n");
				}
				FileUtils.write(new File("corpus_word2vec"), sb.toString(),
						true);
				++i;
				System.out.println("第" + i + "个文件已经合并完成!");
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public static void createCorpusForAncance(String inputDir,
			String unifinedFile) {
		try {
			File dir = new File(inputDir);
			int i = 0;
			for (File file : dir.listFiles()) {
				List<String> lines = FileUtils.readLines(file);
				StringBuilder sb = new StringBuilder();
				for (String line : lines) {
					sb.append(line).append(" ");
				}
				sb.append("\n");
				FileUtils.write(new File(unifinedFile), sb.toString(), true);

				++i;
				System.out.println("第" + i + "个文件已经合并完成!");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
