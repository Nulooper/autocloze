package edu.pku.ss.advance;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import org.apache.commons.io.FileUtils;

public class FileFilter {

	private static List<String> vocabulary = null;
	static {
		try {
			vocabulary = FileUtils
					.readLines(new File("advance/potential_words"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void filter(String inputDir, String outputDir) {
		try {
			File dir = new File(inputDir);
			int i = 0;
			for (File file : dir.listFiles()) {
				List<String> words = FileUtils.readLines(file);
				if (!Collections.disjoint(words, vocabulary)) {
					FileUtils.writeLines(
							new File(outputDir + "/" + file.getName()), words);
				}
				System.out
						.println("The " + (++i) + " file has been processed!");
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
}
