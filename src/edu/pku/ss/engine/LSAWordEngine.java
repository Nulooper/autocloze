package edu.pku.ss.engine;

import java.util.HashMap;
import java.util.Map;

import edu.pku.ss.evaluation.Problem;
import edu.pku.ss.evaluation.Selection;
import edu.pku.ss.helper.PathHelper;
import edu.pku.ss.lsa.LSA;

public class LSAWordEngine implements Engine {

	private static LSAWordEngine instance = new LSAWordEngine();

	private LSA wordLSA;

	private LSAWordEngine() {
		this.wordLSA = LSA.createLSA(PathHelper.EngineModel.LSA_WORD,
				PathHelper.Index.WORD_MATRIX_INDEX, 5685, 300);
	}

	public static LSAWordEngine getInstance() {
		return instance;
	}

	@Override
	public Selection getBest(Problem problem) {
		Map<Selection, Double> selectionSimiMap = new HashMap<Selection, Double>();
		double similarity;
		for (Selection s : problem.selections) {
			similarity = this.wordLSA.getSumSimilarity(s.word,
					problem.filteredWords);
			selectionSimiMap.put(s, similarity);
		}
		return BestSelector.getBest(selectionSimiMap);
	}

}
