package edu.pku.ss.engine;

import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;

import edu.pku.ss.evaluation.Selection;

public class BestSelector {
	public static Selection getBest(Map<Selection, Double> selectionSimiMap) {

		if (selectionSimiMap == null || selectionSimiMap.isEmpty()) {
			Logger.getAnonymousLogger().warning(
					"-----------答案爲空!--------------");
			return null;//
		}

		Map.Entry<Selection, Double> best = Collections.max(
				selectionSimiMap.entrySet(),
				new Comparator<Map.Entry<Selection, Double>>() {

					@Override
					public int compare(Entry<Selection, Double> o1,
							Entry<Selection, Double> o2) {
						return Double.compare(o1.getValue(), o2.getValue());
					}
				});

		return best.getKey();
	}
}
