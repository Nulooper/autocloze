package edu.pku.ss.engine;

import edu.pku.ss.evaluation.Problem;
import edu.pku.ss.evaluation.Selection;

public interface Engine {
	public Selection getBest(Problem problem);
}
