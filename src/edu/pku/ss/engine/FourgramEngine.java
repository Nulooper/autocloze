package edu.pku.ss.engine;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.pku.ss.evaluation.Problem;
import edu.pku.ss.evaluation.Selection;

public class FourgramEngine implements Engine {

	private static FourgramEngine instance = new FourgramEngine();

	private NgramEngine ne;

	private FourgramEngine() {
		// wait to implement
		this.ne = new NgramEngine(4);
	}

	public static FourgramEngine getInstance() {
		return instance;
	}

	@Override
	public Selection getBest(Problem problem) {
		Map<Selection, Double> selectionProbMap = new HashMap<Selection, Double>();
		for (Selection s : problem.selections) {
			List<String> sent = problem.getSentence(s);
			selectionProbMap.put(s, this.ne.getSentenceProb(sent));
		}
		return BestSelector.getBest(selectionProbMap);
	}

}
