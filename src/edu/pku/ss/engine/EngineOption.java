package edu.pku.ss.engine;

public enum EngineOption {
	BIGRAM, TRIGRAM, FOURGRAM, LSA_WORD, LSA_WORD_BIGRAM, WORD_2_VEC
}
