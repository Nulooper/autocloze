package edu.pku.ss.engine;

import java.util.HashMap;
import java.util.Map;

import edu.pku.ss.evaluation.Problem;
import edu.pku.ss.evaluation.Selection;
import edu.pku.ss.helper.PathHelper;
import edu.pku.ss.lsa.LSA;

public class LSAWordBigramEngine implements Engine {

	private static LSAWordBigramEngine instance = new LSAWordBigramEngine();

	private LSA wordBigramLSA;

	private LSAWordBigramEngine() {
		this.wordBigramLSA = LSA.createLSA(
				PathHelper.EngineModel.LSA_WORD_BIGRAM,
				PathHelper.Index.WORD_BIGRAM_MATRIX_INDEX, 17133, 300);
	}

	public static LSAWordBigramEngine getInstance() {
		return instance;
	}

	@Override
	public Selection getBest(Problem problem) {
		Map<Selection, Double> selectionSimiMap = new HashMap<Selection, Double>();
		double similarity;
		for (Selection s : problem.selections) {
			similarity = this.wordBigramLSA.getSumSimilarity(s.word,
					problem.filteredWords);
			selectionSimiMap.put(s, similarity);
		}
		return BestSelector.getBest(selectionSimiMap);
	}

}
