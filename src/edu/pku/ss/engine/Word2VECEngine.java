package edu.pku.ss.engine;

import java.util.HashMap;
import java.util.Map;

import edu.pku.ss.evaluation.Problem;
import edu.pku.ss.evaluation.Selection;
import edu.pku.ss.word2vec.Word2VEC;

public class Word2VECEngine implements Engine {

	private static Word2VECEngine instance = new Word2VECEngine();

	private Word2VEC word2vec;

	private Word2VECEngine() {
		this.word2vec = new Word2VEC();
	}

	public static Word2VECEngine getInstance() {
		return instance;
	}

	@Override
	public Selection getBest(Problem problem) {
		Map<Selection, Double> selectionSimiMap = new HashMap<Selection, Double>();
		double similarity;
		for (Selection s : problem.selections) {
			similarity = this.word2vec.getSumSimilarity(s.word,
					problem.filteredWords);
			selectionSimiMap.put(s, similarity);
		}
		return BestSelector.getBest(selectionSimiMap);
	}
}
