package edu.pku.ss.engine;

import java.util.List;
import edu.berkeley.nlp.lm.ConfigOptions;
import edu.berkeley.nlp.lm.ContextEncodedNgramLanguageModel;
import edu.berkeley.nlp.lm.StringWordIndexer;
import edu.berkeley.nlp.lm.io.*;
import edu.pku.ss.helper.PathHelper;

public class NgramEngine {

	private final StringWordIndexer wordIndexer;
	private final ConfigOptions opts = new ConfigOptions();
	private static ContextEncodedNgramLanguageModel<String> lm;

	public NgramEngine(int order) {
		wordIndexer = new StringWordIndexer();
		wordIndexer.setStartSymbol("<s>");
		wordIndexer.setEndSymbol("</s>");
		wordIndexer.setUnkSymbol("<unk>");
		lm = LmReaders.readContextEncodedLmFromArpa(
				PathHelper.EngineModel.NGRAM, wordIndexer, opts, order);
	}

	public double getSentenceProb(List<String> sentence) {
		if (sentence == null || sentence.isEmpty())
			return 0;
		else
			return lm.scoreSentence(sentence);
	}
}
