package edu.pku.ss.engine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.pku.ss.combination.Vote;
import edu.pku.ss.evaluation.Problem;
import edu.pku.ss.evaluation.Selection;

public class AutoCloze {

	private static AutoCloze instance = new AutoCloze();

	private Map<EngineOption, Engine> engineMap = new HashMap<EngineOption, Engine>();

	private AutoCloze() {
		this.engineMap.put(EngineOption.BIGRAM, BigramEngine.getInstance());
		System.out.println("BIGRAM引擎加载结束！");
		this.engineMap.put(EngineOption.TRIGRAM, TrigramEngine.getInstance());
		System.out.println("TRIGRAM引擎加载结束！");
		this.engineMap.put(EngineOption.FOURGRAM, FourgramEngine.getInstance());
		System.out.println("FOURGRAM引擎加载结束！");
		this.engineMap.put(EngineOption.LSA_WORD, LSAWordEngine.getInstance());
		System.out.println("LSA_WORD引擎加载结束！");
		this.engineMap.put(EngineOption.LSA_WORD_BIGRAM,
				LSAWordBigramEngine.getInstance());
		System.out.println("LSA_WORD_BIGRAM引擎加载结束！");
		this.engineMap.put(EngineOption.WORD_2_VEC,
				Word2VECEngine.getInstance());
		System.out.println("WORD_2_VEC引擎加载结束！");
	}

	public static AutoCloze getInstance() {
		return instance;
	}

	public Selection getBestByVote(Problem problem) {
		List<Selection> selections = new ArrayList<Selection>();
		int i = 0;
		for (EngineOption key : this.engineMap.keySet()) {
			selections.add(this.engineMap.get(key).getBest(problem));
			System.out.println("第" + (++i) + "个引擎分析结束!");
		}
		return Vote.getVoteResult(selections);
	}

	/**
	 * 使用单一引擎来对指定的problem，进行完型填空.
	 * 
	 * @param option
	 *            指定要使用的单一的自动完型填空引擎.
	 * @param problem
	 *            要解决的问题.
	 * @return 返回指定单一引擎预测的最佳选项.
	 */
	public Selection getBestBySingle(EngineOption option, Problem problem) {
		Engine engine = this.engineMap.get(option);
		return engine.getBest(problem);
	}

	public Engine getSingleEngine(EngineOption option) {
		return this.engineMap.get(option);
	}
}
