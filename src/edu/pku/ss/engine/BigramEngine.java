package edu.pku.ss.engine;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.pku.ss.evaluation.Problem;
import edu.pku.ss.evaluation.Selection;

public class BigramEngine implements Engine {

	private static BigramEngine instance = new BigramEngine();

	private NgramEngine ne;

	private BigramEngine() {
		this.ne = new NgramEngine(2);
	}

	public static BigramEngine getInstance() {
		return instance;
	}

	@Override
	public Selection getBest(Problem problem) {
		// TODO Auto-generated method stub
		Map<Selection, Double> selectionProbMap = new HashMap<Selection, Double>();
		for (Selection s : problem.selections) {
			List<String> sent = problem.getSentence(s);
			selectionProbMap.put(s, this.ne.getSentenceProb(sent));
		}
		return BestSelector.getBest(selectionProbMap);
	}

}
