package edu.pku.ss.combination;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.pku.ss.evaluation.Selection;

public class Vote {
	/**
	 * 使用投票算法对6个模型得出的结果进行投票，选出得票最高的选项作为我们的最终答案.
	 * 
	 * @param selections
	 * @return
	 */
	public static Selection getVoteResult(List<Selection> selections) {
		Map<Selection, Integer> selectionVoteMap = new HashMap<Selection, Integer>();
		for (Selection s : selections) {
			if (selectionVoteMap.containsKey(s)) {
				selectionVoteMap.put(s, selectionVoteMap.get(s) + 1);
			} else {
				selectionVoteMap.put(s, 1);
			}
		}

		int maxNum = 0;
		Selection best = null;
		for (Selection key : selectionVoteMap.keySet()) {
			if (maxNum < selectionVoteMap.get(key)) {
				maxNum = selectionVoteMap.get(key);
				best = key;
			}
		}

		return best;
	}
}
