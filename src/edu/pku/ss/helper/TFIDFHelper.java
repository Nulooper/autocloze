package edu.pku.ss.helper;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;

import edu.pku.ss.nlp.toolkit.Tokenizer;
import edu.pku.ss.preprocess.Constants;

public class TFIDFHelper {

	private static final Logger logger = Logger
			.getLogger("edu.pku.ss.utils.TFIDFUtils");

	/**
	 * 统计inputDir目录下面每一个文档中每个单词的TF值.
	 * 
	 * 注意：inputDir目录下面的每一个文件中的格式为："brave	20", 即word+"\t"+freq.
	 * outputDir目录下面的每一个文件的格式为：word+"\t"+TF.
	 * 
	 * 
	 * @param inputDir
	 *            统计好频率的输入目录.
	 * @param outputDir
	 *            计算TF之后的输出目录.
	 * @param encoding
	 *            输入与输出文件的编码.
	 */
	public static void computeTF(String inputDir, String outputDir,
			Charset encoding) {
		try {
			File dir = new File(inputDir);
			int count = 0; // 计数，仅仅用来显示计算进度.
			for (File f : dir.listFiles()) {

				count++;// 计数，仅仅用来显示计算进度.

				List<String> lines = FileUtils.readLines(f, encoding);
				Map<String, Integer> wordFreqMap = new HashMap<String, Integer>();
				Map<String, Double> tfMap = new HashMap<String, Double>();

				String[] two = null;
				String key = null;
				int value = 0;
				int sum = 0;// 总词数。
				for (String line : lines) {
					two = line.split("\t");
					key = two[0];
					value = Integer.parseInt(two[1]);
					sum += value;
					wordFreqMap.put(key, value);
				}

				for (String word : wordFreqMap.keySet()) {
					tfMap.put(word, wordFreqMap.get(word) * 1.0 / sum);
				}

				IOHelper.writeMapToFile(tfMap,
						new File(outputDir + "/" + f.getName()));

				logger.info("已经处理了第" + count + "文件!" + ": " + f.getName());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 统计文档集合中包含某一个单词的文档数量.即产生一个文件，文件中每一个单词和对应的包含该单词的文档数量。
	 */
	public static void createWordSetNum(String inputMatrixVocabIndex,
			String inputFreqMatrixPath, String outputSumPath) {
		try {

			// 读取词表,构建hashmap
			List<String> words = FileUtils.readLines(new File(
					PathHelper.Word.FILE_VOCABULARY));
			Map<String, Integer> wordDocsMap = new HashMap<String, Integer>();
			for (String word : words) {
				wordDocsMap.put(word, 0);
			}

			// 读取索引，用来确定索引对应的单词
			List<String> indexes = FileUtils.readLines(new File(
					inputMatrixVocabIndex));
			String[] wordIndex = null;
			Map<Integer, String> indexWordMap = new HashMap<Integer, String>();
			for (String index : indexes) {
				wordIndex = index.split("\t");
				indexWordMap.put(Integer.parseInt(wordIndex[1]), wordIndex[0]);
			}

			List<String> lines = FileUtils.readLines(new File(
					inputFreqMatrixPath));
			for (int i = 0; i < Constants.wordCount; ++i) {
				String line = lines.get(i);
				String[] items = line.split("\t");
				String key = indexWordMap.get(i);
				for (int j = 0; j < Constants.docSetCount; ++j) {
					if (Integer.parseInt(items[j]) != 0) {
						wordDocsMap.put(key, wordDocsMap.get(key) + 1);
					}
				}
				System.out.println("已经处理第" + i + "行矩阵！");
			}

			// 写入文件
			IOHelper.writeMapToFile(wordDocsMap, new File(outputSumPath));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 计算每一篇文档中在词表中的单词的tfidf值。
	 */
	public static void createWordVocabTFIDF(String inputVocabSetFreqFile,
			String inputTFDir, String outputTFIDFDir) {
		try {
			List<String> vocab = FileUtils.readLines(new File(
					PathHelper.Word.FILE_VOCABULARY));

			// 构建map，确定每一个单词出现的文档数量。
			Map<String, Integer> wordSetNumMap = new HashMap<String, Integer>();
			File file = new File(inputVocabSetFreqFile);
			List<String> wordNumLines = FileUtils.readLines(file);
			String[] two = null;
			for (String line : wordNumLines) {
				two = line.split("\t");
				wordSetNumMap.put(two[0], Integer.parseInt(two[1]));
			}

			File dir = new File(inputTFDir);
			int count = 0;
			for (File f : dir.listFiles()) {
				count++;
				Map<String, Double> tfidf = new HashMap<String, Double>();
				List<String> lines = FileUtils.readLines(f);
				String[] twoItems = null;
				String word = null;
				double tf = 0.0;
				double idf = 0.0;
				for (String line : lines) {
					twoItems = line.split("\t");
					word = twoItems[0];
					tf = Double.parseDouble(twoItems[1]);
					if (vocab.contains(word)) {
						idf = Math.log(Constants.docSetCount * 1.0
								/ (wordSetNumMap.get(word) + 1))
								/ Math.log(2);
						tfidf.put(word, tf * idf);
					}
				}
				IOHelper.writeMapToFile(tfidf, new File(outputTFIDFDir + "/"
						+ f.getName()));
				System.out.println("处理第" + count + "篇文档完成！");
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 为每一篇文档创建一个词频统计词典.
	 */
	public static void createWordFreqMap(String inputWordVectorDir,
			String outputWordFreqMapDir) {
		try {
			File dir = new File(inputWordVectorDir);
			int i = 0;
			for (File f : dir.listFiles()) {
				++i;
				Map<String, Integer> tmpMap = new HashMap<String, Integer>();
				List<String> words = FileUtils.readLines(f);
				for (String word : words) {
					if (tmpMap.containsKey(word)) {
						tmpMap.put(word, tmpMap.get(word) + 1);
					} else {
						tmpMap.put(word, 1);
					}
				}
				IOHelper.writeMapToFile(tmpMap, new File(outputWordFreqMapDir
						+ "/" + f.getName()));
				System.out.println("处理第" + i + "篇文档完成");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 构造数据集中的词表；将数据集中的每一个文档用词向量表示。
	 */
	public static void createVocabAndWordVec(String inputCorpusDir,
			String outputDir, String outputVocabPath) {
		try {
			File dir = new File(inputCorpusDir);
			Set<String> vocabulary = new HashSet<String>();
			int i = 0;
			if (dir.isDirectory()) {
				for (File f : dir.listFiles()) {
					++i;
					String text = FileUtils.readFileToString(f);
					String[] items = Tokenizer.getInstance().tokenize(text);
					List<String> itemList = new ArrayList<String>();
					for (String item : items) {
						itemList.add(item.toLowerCase());
					}
					vocabulary.addAll(itemList);
					FileUtils.writeLines(
							new File(outputDir + "/word_" + f.getName()),
							itemList);
					System.out.println("处理完成第" + i + "篇文档！");
				}
				FileUtils.writeLines(new File(outputVocabPath), vocabulary);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
