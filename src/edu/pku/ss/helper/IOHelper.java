package edu.pku.ss.helper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;

public class IOHelper {
	/**
	 * 将给定的map,以每行key+"\t"+value的形式写入指定文件file.
	 * 
	 * @param mapToWrite
	 * @param file
	 */
	public static <V> void writeMapToFile(Map<String, V> mapToWrite, File file) {
		try {
			List<String> wordFreqLines = new ArrayList<String>();
			for (String key : mapToWrite.keySet()) {
				wordFreqLines.add(key + "\t" + mapToWrite.get(key));
			}
			FileUtils.writeLines(file, wordFreqLines);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
