package edu.pku.ss.helper;

import java.io.BufferedReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Logger;

public class MatrixHelper {

	private static final Logger logger = Logger
			.getLogger("edu.pku.ss.utils.MatrixUtils");

	/**
	 * 矩阵文件中，元素之间的间隔符号.
	 */
	private static final String SEPERATOR_TWO_SPACE = "  ";

	public static double[][] readMatrix(String matrixPath, int row, int column,
			Charset encoding) {
		double[][] matrix = new double[row][column];
		if (matrixPath == null || matrixPath.isEmpty() || row <= 0
				|| column <= 0) {
			logger.warning("读取矩阵文件:" + matrixPath + "失败！程序退出！");
			System.exit(0);
			return matrix;
		}

		try (BufferedReader reader = Files.newBufferedReader(
				Paths.get(matrixPath), encoding)) {
			String line = null;
			logger.info("开始读取矩阵！");
			int i = 0;

			while ((line = reader.readLine()) != null) {
				String[] items = line.split(SEPERATOR_TWO_SPACE);
				for (int j = 0; j < column; ++j) {
					matrix[i][j] = Double.parseDouble(items[j + 1]);
				}
				++i;
			}

			logger.info("矩陣读取结束！");
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return matrix;
	}
}
