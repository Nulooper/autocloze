package edu.pku.ss.helper;

import java.io.File;

public class PathHelper {

	public static final String DIR = "resource";

	public static final class EngineModel {
		public static final String NGRAM = DIR + File.separator + "ngram"
				+ File.separator + "lm4.lm";
		public static final String LSA_WORD = DIR + File.separator + "lsa"
				+ File.separator + "word_matrix_5685_300.txt";
		public static final String LSA_WORD_BIGRAM = DIR + File.separator
				+ "lsa" + File.separator + "word_bigram_matrix_17133_300.txt";
		public static final String WORD_2_VEC_RAW = DIR + File.separator
				+ "word2vec" + File.separator + "raw_corpus_word_vec";
		public static final String WORD_2_VEC_PROCESSED = DIR + File.separator
				+ "word2vec" + File.separator + "processed_valid_word_vec";
	}

	public static final class Vocabulary {
		public static final String VOCABULARY = DIR + File.separator
				+ "vocabulary" + File.separator
				+ "all_devep_test_word_vocabulary";
	}

	public static final class Index {
		public static final String WORD_MATRIX_INDEX = DIR + File.separator
				+ "lsa" + File.separator + "word_matrix_index";
		public static final String WORD_BIGRAM_MATRIX_INDEX = DIR
				+ File.separator + "lsa" + File.separator
				+ "word_bigram_matrix_index";
	}

	public static final class Corpus {
		public static final String DIR_BASE = DIR + File.separator + "corpus";

		public static final String TRINING_DATA_1 = DIR_BASE + File.separator
				+ "Training_Data_1";
	}

	public static final class Clean {
		public static final String DIR_BASE = DIR + File.separator + "clean";

		public static final String DIR_CHAPTER_DATA = DIR_BASE + File.separator
				+ "chapter_data";
		public static final String DIR_NO_BLANK_DATA = DIR_BASE
				+ File.separator + "no_blank_data";
		public static final String DIR_SENTENCED_DATA = DIR_BASE
				+ File.separator + "sentenced_data";
		public static final String DIR_SENTENCED_NGRAM_DATA = DIR_BASE
				+ File.separator + "sentenced_ngram_data";
	}

	public static final class Bigram {
		public static final String DIR_BASE = DIR + File.separator + "bigram";

		public static final String FILE_VOCABULARY = DIR_BASE + File.separator
				+ "bigram_vocabulary";
		public static final String FILE_SET_FREQ = DIR_BASE + File.separator
				+ "bigram_set_freq";
		public static final String FILE_BIGRAM_DOC_FREQ_MATRIX = DIR_BASE
				+ File.separator + "bigram_doc_freq_matrix";
		public static final String FILE_BIGRAM_DOC_TFIDF_MATRIX = DIR_BASE
				+ File.separator + "bigram_doc_tfidf_matrix";
		public static final String FILE_FREQ_MATRIX_INDEX = DIR_BASE
				+ File.separator + "bigram_freq_matrix_index";
		public static final String FILE_TFIDF_MATRIX_INDEX = DIR_BASE
				+ File.separator + "bigram_tfidf_matrix_index";

		public static final String DIR_BIGRAM_FREQ_DATA = DIR_BASE
				+ File.separator + "bigram_freq_data";
		public static final String DIR_BIGRAM_TF_DATA = DIR_BASE
				+ File.separator + "bigram_tf_data";
		public static final String DIR_BIGRAM_TFIDF_DATA = DIR_BASE
				+ File.separator + "bigram_tfidf_data";
	}

	public static final class Trigram {
		public static final String DIR_BASE = DIR + File.separator + "trigram";

		public static final String FILE_VOCABULARY = DIR_BASE + File.separator
				+ "trigram_vocabulary";
		public static final String FILE_SET_FREQ = DIR_BASE + File.separator
				+ "set_freq";
		public static final String FILE_TRIGRAM_DOC_FREQ_MATRIX = DIR_BASE
				+ File.separator + "trigram_doc_freq_matrix";
		public static final String FILE_TRIGRAM_DOC_TFIDF_MATRIX = DIR_BASE
				+ File.separator + "trigram_doc_tfidf_matrix";
		public static final String FILE_FREQ_MATRIX_INDEX = DIR_BASE
				+ File.separator + "freq_matrix_index";
		public static final String FILE_TFIDF_MATRIX_INDEX = DIR_BASE
				+ File.separator + "tfidf_matrix_index";

		public static final String DIR_TRIGRAM_FREQ_DATA = DIR_BASE
				+ File.separator + "trigram_freq_data";
		public static final String DIR_TRIGRAM_TF_DATA = DIR_BASE
				+ File.separator + "trigram_tf_data";
		public static final String DIR_TRIGRAM_TFIDF_DATA = DIR_BASE
				+ File.separator + "trigram_tfidf_data";
	}

	public static final class Word {
		public static final String DIR_BASE = DIR + File.separator + "word";

		public static final String FILE_VOCABULARY = DIR_BASE + File.separator
				+ "word_vocabulary";
		public static final String FILE_SET_FREQ = DIR_BASE + File.separator
				+ "word_set_freq";
		public static final String FILE_WORD_DOC_FREQ_MATRIX = DIR_BASE
				+ File.separator + "word_doc_freq_matrix";
		public static final String FILE_WORD_DOC_TFIDF_MATRIX = DIR_BASE
				+ File.separator + "word_doc_tfidf_matrix";
		public static final String FILE_FREQ_MATRIX_INDEX = DIR_BASE
				+ File.separator + "freq_matrix_index";
		public static final String FILE_TFIDF_MATRIX_INDEX = DIR_BASE
				+ File.separator + "tfidf_matrix_index";

		public static final String DIR_WORD_FREQ_DATA = DIR_BASE
				+ File.separator + "word_freq_data";
		public static final String DIR_WORD_TF_DATA = DIR_BASE + File.separator
				+ "word_tf_data";
		public static final String DIR_VOCAB_WORD_TFIDF_DATA = DIR_BASE
				+ File.separator + "word_vocab_tfidf_data";
	}

	public static final class SimiVersion1 {
		public static final String DIR_BASE = DIR + File.separator
				+ "similarity1";

		public static final String FILE_SIMI_MATRIX_1 = DIR_BASE
				+ File.separator + "";
		public static final String FILE_SIMI_MATRIX_2 = DIR_BASE
				+ File.separator + "";
		public static final String FILE_SIMI_MATRIX_3 = DIR_BASE
				+ File.separator + "";
		public static final String FILE_SIMI_MATRIX_4 = DIR_BASE
				+ File.separator + "";

	}

	public static final class SimiVersion2 {
		public static final String DIR_BASE = DIR + File.separator
				+ "similarity2";

		public static final String FILE_SIMI_MATRIX_1 = DIR_BASE
				+ File.separator + "";
		public static final String FILE_SIMI_MATRIX_2 = DIR_BASE
				+ File.separator + "";
		public static final String FILE_SIMI_MATRIX_3 = DIR_BASE
				+ File.separator + "";
		public static final String FILE_SIMI_MATRIX_4 = DIR_BASE
				+ File.separator + "";

	}

	public static final class SimiVersion3 {
		public static final String DIR_BASE = DIR + File.separator
				+ "similarity3";

		public static final String FILE_SIMI_MATRIX_1 = DIR_BASE
				+ File.separator + "v3_simi_17133_200.txt";
		public static final String FILE_SIMI_MATRIX_2 = DIR_BASE
				+ File.separator + "v3_simi_17133_300.txt";
		public static final String FILE_SIMI_MATRIX_3 = DIR_BASE
				+ File.separator + "v3_simi_17133_1000.txt";
		public static final String FILE_SIMI_MATRIX_4 = DIR_BASE
				+ File.separator + "";

	}

	public static final class SimiVersion4 {
		public static final String DIR_BASE = DIR + File.separator
				+ "similarity4";

		public static final String FILE_SIMI_MATRIX_1 = DIR_BASE
				+ File.separator + "re_A_5685_200";
		public static final String FILE_SIMI_MATRIX_2 = DIR_BASE
				+ File.separator + "re_A_5685_300";
		public static final String FILE_SIMI_MATRIX_3 = DIR_BASE
				+ File.separator + "re_A_5685_522";
		public static final String FILE_SIMI_MATRIX_4 = DIR_BASE
				+ File.separator + "re_A_5685_100";

	}

	public static final class Development {
		public static final String DIR_BASE = DIR + File.separator + "develop";

		public static final String FILE_DEVELOP_PROBLEM = DIR_BASE
				+ File.separator + "development_set.txt";
		public static final String FILE_DEVELOP_PROBLEM_ANSWERS = DIR_BASE
				+ File.separator + "development_set_answers.txt";
	}

	public static final class Test {
		public static final String DIR_BASE = DIR + File.separator + "test";

		public static final String FILE_TEST_PROBLEM = DIR_BASE
				+ File.separator + "test_set.txt";
	}

	public static final class StopWord {
		public static final String DIR_BASE = DIR + File.separator + "stp";

		public static final String STOP_WORDS = DIR_BASE + File.separator
				+ "stopword";
	}

	public static final class Merge {
		public static final String DIR_BASE = DIR + File.separator + "merge";

		public static final String FILE_WORD_BIGRAM_VOCAB = DIR_BASE
				+ File.separator + "word_bigram_vocab";
		public static final String FILE_WORD_BIGRAM_MATRIX_INDEX = DIR_BASE
				+ File.separator + "word_bigram_matrix_index";
		public static final String FILE_WORD_BIGRAM_DOC_TFIDF_MATRIX = "word_bigram_doc_tfidf_matrix";
	}
}
