package edu.pku.ss.helper;

public class StringHelper {

	/**
	 * 将double数组转为一个字符串对象，每个数组元素之间使用"\t"进行分割.
	 * 
	 * @param items
	 * @return
	 */
	public static String stringFromArray(double[] items) {
		StringBuilder sb = new StringBuilder();

		int size = items.length;
		int i;
		for (i = 0; i < size - 1; ++i) {
			sb.append(items[i]).append("\t");
		}
		sb.append(items[i]);
		sb.append("\n");

		return sb.toString();
	}

	/**
	 * 将int数组转为一个字符串对象，每个数组元素之间使用"\t"进行分割.
	 * 
	 * @param items
	 * @return
	 */
	public static String stringFromArray(int[] items) {
		StringBuilder sb = new StringBuilder();

		int size = items.length;
		int i;
		for (i = 0; i < size - 1; ++i) {
			sb.append(items[i]).append("\t");
		}
		sb.append(items[i]);
		sb.append("\n");

		return sb.toString();
	}
}
