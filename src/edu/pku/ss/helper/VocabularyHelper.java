package edu.pku.ss.helper;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;

import edu.pku.ss.evaluation.EvaluatorHelper;
import edu.pku.ss.evaluation.Problem;

public class VocabularyHelper {

	/**
	 * 创建词表，为了简化LSA共现矩阵的数量，仅仅需要统计developmen_set和test_set中出现的单词,即可.
	 */
	public static void createWordVocabulary() {
		try {
			Set<String> vocabulary = new HashSet<String>();

			List<Problem> developments = EvaluatorHelper.readProblems(
					PathHelper.Development.FILE_DEVELOP_PROBLEM,
					Charset.forName("UTF-8"));
			for (Problem p : developments) {
				vocabulary.addAll(p.totalWords);
			}

			List<Problem> tests = EvaluatorHelper
					.readProblems(PathHelper.Test.FILE_TEST_PROBLEM,
							Charset.forName("UTF-8"));
			for (Problem p : tests) {
				vocabulary.addAll(p.totalWords);
			}
			FileUtils.writeLines(new File("vocab"), new ArrayList<String>(
					vocabulary));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void createStopList() {
		try {
			List<String> lines = FileUtils.readLines(new File(
					PathHelper.Word.FILE_SET_FREQ));
			Collections.sort(lines, new Comparator<String>() {

				@Override
				public int compare(String o1, String o2) {
					String[] items1 = o1.split("\t");
					String[] items2 = o2.split("\t");
					return Integer.parseInt(items1[1])
							- Integer.parseInt(items2[1]);
				}

			});

			List<String> new_lines = new ArrayList<String>();
			List<String> stopList = lines.subList(5475, lines.size());
			for (String line : stopList) {
				new_lines.add(line.split("\t")[0]);
			}

			FileUtils.writeLines(new File("new_stop"), lines);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
