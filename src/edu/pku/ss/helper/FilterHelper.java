package edu.pku.ss.helper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.io.FileUtils;

public class FilterHelper {
	private static List<String> stopList = null;
	static {
		try {
			stopList = FileUtils.readLines(new File("function.stp"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static List<String> filter(List<String> wordList) {
		if (wordList == null || wordList.isEmpty()) {
			return Collections.emptyList();
		}

		List<String> validList = new ArrayList<String>();
		for (String word : wordList) {
			if (!stopList.contains(word)) {
				validList.add(word);
			}
		}
		return Collections.unmodifiableList(validList);
	}

	public static List<String> filter(String[] words) {
		if (words == null || words.length == 0) {
			return Collections.emptyList();
		}
		return filter(Arrays.asList(words));
	}
}
