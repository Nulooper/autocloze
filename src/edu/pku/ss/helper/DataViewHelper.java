package edu.pku.ss.helper;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

import edu.pku.ss.evaluation.EvaluatorHelper;
import edu.pku.ss.evaluation.Problem;

public class DataViewHelper {
	public static void viewProblems() {
		try {
			List<Problem> problemList = EvaluatorHelper.readProblems(
					PathHelper.Development.FILE_DEVELOP_PROBLEM,
					Charset.forName("UTF-8"));
			int sumLeft = 0;
			int sumRight = 0;
			for (Problem p : problemList) {
				System.out.println(p.id + ":\t" + p.leftWords.size() + ", "
						+ p.rightWords.size());
				sumLeft += p.leftWords.size();
				sumRight += p.rightWords.size();
			}
			System.out.println(sumLeft * 1.0 / problemList.size() + "\t"
					+ sumRight * 1.0 / problemList.size());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
