package edu.pku.ss.helper;

import java.util.logging.Logger;

public class MathHelper {

	private static final Logger logger = Logger
			.getLogger("edu.pku.ss.utils.MathUtils");

	public static double cosine(double[] vec1, double[] vec2) {
		if (vec1 == null || vec2 == null || vec1.length == 0
				|| vec2.length == 0 || vec1.length != vec2.length) {
			logger.warning("vec1和vec2输入不合法，返回值默认值0！");
			return 0;
		}

		int length = vec1.length;
		double result = 0.0;
		for (int i = 0; i < length; ++i) {
			result += vec1[i] * vec2[i];
		}

		double normVer1 = getNorm(vec1);
		double normVer2 = getNorm(vec2);

		if (Double.compare(normVer1, 0.0) == 0
				|| Double.compare(normVer2, 0.0) == 0) {
			logger.warning("两个向量有一个为0向量，返回默认值0!");
			return 1.0;
		}
		return result / (normVer1 * normVer2);
	}

	/**
	 * 计算给定double类型数组的范式.
	 * 
	 * @param vec
	 *            以数组形式表现的向量.
	 * @return 返回向量的范数，正值。
	 */
	private static double getNorm(double[] vec) {
		if (vec == null || vec.length == 0) {
			logger.warning("输入向量vec为0向量，返回默认值1.0!");
			return 1.0;
		}
		double length = 0.0;
		for (double value : vec) {
			length += Math.pow(value, 2.0);
		}
		return Math.sqrt(length);
	}
}
