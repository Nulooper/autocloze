package edu.pku.ss.preprocess;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;

public class WordAndBigram {

	/**
	 * 将word和bigram组合成一个新的词表
	 */
	public static void createVocabOfWordAndBigram() {
		try {
			List<String> wordVocabulary = FileUtils.readLines(new File(
					"preprocess/vocabulary/develop_vocabulary"));
			List<String> bigramVOcabulary = FileUtils.readLines(new File(
					"preprocess/new_develop_test_ngram/bigram"));

			List<String> wordAndBigramVocabulary = new ArrayList<String>();
			wordAndBigramVocabulary.addAll(wordVocabulary);
			wordAndBigramVocabulary.addAll(bigramVOcabulary);

			FileUtils.writeLines(new File(
					"preprocess/merge/word_and_bigram_vocab"),
					wordAndBigramVocabulary);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 为word和bigram组合成心的词表，创建索引.
	 */
	public static void createNewVocabIndex() {
		try {
			List<String> items = FileUtils
					.readLines(new File(
							"preprocess/new_develop_test_ngram/bigram_matrix_tfidf_index"));
			List<String> itemIndexes = new ArrayList<String>();
			String[] two = null;
			for (String item : items) {
				two = item.split("\t");
				itemIndexes.add(two[0] + "\t"
						+ (Integer.parseInt(two[1]) + 5685));
			}

			FileUtils.writeLines(new File(
					"preprocess/merge/word_bigram_matrix_index"), itemIndexes,
					true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 将bigram构建的共现矩阵append至word共现矩阵文件。从形成一个新的共现矩阵文件。
	 */
	public static void mergeMatrix() {
		try {
			List<String> rowList = FileUtils
					.readLines(new File(
							"preprocess/new_develop_test_ngram/bigram_doc_tfidf_matrix"));
			FileUtils.writeLines(new File(
					"preprocess/merge/term_bigram_doc_tfidf_matrix"), rowList,
					true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
