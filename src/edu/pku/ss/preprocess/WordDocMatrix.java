package edu.pku.ss.preprocess;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;

import edu.pku.ss.helper.IOHelper;
import edu.pku.ss.helper.PathHelper;
import edu.pku.ss.helper.StringHelper;

public class WordDocMatrix {

	/**
	 * 构建单词*文档共现矩阵.
	 * 
	 * @throws IOException
	 */
	public static void createTermDocMatrix() throws IOException {

		// 第一步：建议一个map，键为文件名，值为该文件的单词词频map.
		Map<String, Map<String, Double>> docWordFreqMap = createDocWordFreqMap();

		List<String> vocabulary = FileUtils.readLines(new File(
				PathHelper.Word.FILE_VOCABULARY));
		Map<String, double[]> wordDocFreqMap = new HashMap<String, double[]>();
		int i = 0;
		double[] row = null;

		for (String word : vocabulary) {
			row = new double[Constants.docSetCount];
			Set<String> keySet = docWordFreqMap.keySet();
			int j = 0;
			for (String doc : keySet) {
				if (docWordFreqMap.get(doc).containsKey(word)) {
					row[j] = docWordFreqMap.get(doc).get(word);
				} else {
					row[j] = 0;
				}
				++j;
			}

			wordDocFreqMap.put(word, row);
			System.out.println("正在处理第" + (i + 1) + "单词");
			++i;
		}

		// // 将构建好的单词-文档矩阵写入文件。
		int line = 0;
		Map<String, Integer> wordFreqMatrixIndex = new HashMap<String, Integer>();
		for (String word : wordDocFreqMap.keySet()) {
			wordFreqMatrixIndex.put(word, line);
			++line;
			double[] docFreqMap = wordDocFreqMap.get(word);

			FileUtils.write(new File(
					"preprocess/matrix/term_doc_tfidf_matrix_522"),
					StringHelper.stringFromArray(docFreqMap), true);
			System.out.println(line);
		}
		IOHelper.writeMapToFile(wordFreqMatrixIndex, new File(
				"preprocess/word_tfidf_matrix_index_522"));
	}

	/*
	 * 构建一个字典，key是文件名，value是key对应的文件中的单词*tfidf。
	 */
	private static Map<String, Map<String, Double>> createDocWordFreqMap()
			throws IOException {
		Map<String, Map<String, Double>> docWordFreqMap = new HashMap<String, Map<String, Double>>();
		File dir = new File("preprocess/word_vocab_tfidf_522");
		if (dir.isDirectory()) {
			for (File f : dir.listFiles()) {
				Map<String, Double> wordFreqMap = new HashMap<String, Double>();
				List<String> lines = FileUtils.readLines(f);
				for (String line : lines) {
					String[] two = line.split("\t");
					wordFreqMap.put(two[0], Double.parseDouble(two[1]));
				}
				docWordFreqMap.put(f.getName(), wordFreqMap);
			}
		}
		return docWordFreqMap;
	}

}
