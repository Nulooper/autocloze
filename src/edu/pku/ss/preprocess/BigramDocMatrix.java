package edu.pku.ss.preprocess;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;

import edu.pku.ss.helper.IOHelper;

public class BigramDocMatrix {

	/**
	 * 爲bigram创建索引，这里的bigram来自于development_set和test_set。
	 */
	public static void createBigramIndex() {
		try {
			File vocab = new File(
					"preprocess/new_develop_test_ngram/bigram_freq/bigram");
			List<String> vocabulary = FileUtils.readLines(vocab);
			int i = 0;
			for (String term : vocabulary) {
				FileUtils
						.write(new File(
								"preprocess/new_develop_test_ngram/bigram_freq/bigram_index"),
								term + "\t" + i + "\n", true);
				++i;
				System.out.println("第" + i + "二元组处理完成!");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 构建bigram*文档共现矩阵.
	 * 
	 * @throws IOException
	 */
	public static void createBigramDocMatrix() throws IOException {

		// 第一步：建议一个map，键为文件名，值为该文件的单词词频map.
		Map<String, Map<String, Double>> docBigramFreqMap = createDocBigramFreqMap();

		List<String> vocabulary = FileUtils.readLines(new File(
				"preprocess/new_develop_test_ngram/bigram_freq/bigram"));
		Map<String, double[]> bigramDocFreqMap = new HashMap<String, double[]>();
		int i = 0;
		double[] row = null;

		for (String word : vocabulary) {
			row = new double[Constants.docSetCount];
			Set<String> keySet = docBigramFreqMap.keySet();
			int j = 0;
			for (String doc : keySet) {
				if (docBigramFreqMap.get(doc).containsKey(word)) {
					row[j] = docBigramFreqMap.get(doc).get(word);
				} else {
					row[j] = 0;
				}
				++j;
			}

			bigramDocFreqMap.put(word, row);
			System.out.println("正在处理第" + (i + 1) + "bigram");
			++i;
		}

		// // 将构建好的单词-文档矩阵写入文件。
		int line = 0;
		Map<String, Integer> bigramIndexMap = new HashMap<String, Integer>();
		for (String word : bigramDocFreqMap.keySet()) {
			bigramIndexMap.put(word, line);
			++line;
			double[] docFreqMap = bigramDocFreqMap.get(word);

			FileUtils
					.write(new File(
							"preprocess/new_develop_test_ngram/bigram_doc_tfidf_matrix"),
							stringFromArray(docFreqMap), true);

			System.out.println(line);
		}
		IOHelper.writeMapToFile(bigramIndexMap, new File(
				"preprocess/bigram_matrix_tfidf_index"));
	}

	/*
	 * 构建一个字典，key是文件名，value是key对应的文件中的bigram頻率。
	 */
	private static Map<String, Map<String, Double>> createDocBigramFreqMap()
			throws IOException {
		Map<String, Map<String, Double>> docBigramFreqMap = new HashMap<String, Map<String, Double>>();
		File dir = new File("preprocess/new_set_doc_bigram_tfidf");
		if (dir.isDirectory()) {
			for (File f : dir.listFiles()) {
				Map<String, Double> wordFreqMap = new HashMap<String, Double>();
				List<String> lines = FileUtils.readLines(f);
				for (String line : lines) {
					String[] two = line.split("\t");
					wordFreqMap.put(two[0], Double.parseDouble(two[1]));
				}
				docBigramFreqMap.put(f.getName(), wordFreqMap);
			}
		}
		return docBigramFreqMap;
	}

	public static String stringFromArray(int[] row) {
		StringBuilder sb = new StringBuilder();
		for (int item : row) {
			sb.append(item + "\t");
		}
		return sb.toString() + "\n";
	}

	public static String stringFromArray(double[] row) {
		StringBuilder sb = new StringBuilder();
		for (double item : row) {
			sb.append(item + "\t");
		}
		return sb.toString() + "\n";
	}

	/**
	 * 统计文档集合中包含某一个二元組的文档数量.即产生一个文件，文件中每一个二元組和对应的包含该二元組的文档数量。
	 */
	public static void createBigramSetNum() {
		try {

			// 读取词表,构建hashmap
			List<String> bigrams = FileUtils.readLines(new File(
					"preprocess/new_develop_test_ngram/bigram_freq/bigram"));
			Map<String, Integer> bigramDocsMap = new HashMap<String, Integer>();
			for (String word : bigrams) {
				bigramDocsMap.put(word, 0);
			}

			// 读取索引，用来确定索引对应的单词
			List<String> indexes = FileUtils.readLines(new File(
					"preprocess/new_develop_test_ngram/bigram_matrix_index"));
			String[] bigramIndex = null;
			Map<Integer, String> indexBigramMap = new HashMap<Integer, String>();
			for (String index : indexes) {
				bigramIndex = index.split("\t");
				indexBigramMap.put(Integer.parseInt(bigramIndex[1]),
						bigramIndex[0]);
			}

			List<String> lines = FileUtils
					.readLines(new File(
							"preprocess/new_develop_test_ngram/bigram_doc_freq_matrix"));
			for (int i = 0; i < 11448; ++i) {
				String line = lines.get(i);
				String[] items = line.split("\t");
				String key = indexBigramMap.get(i);
				int frequency = 0;
				for (int j = 0; j < 6679; ++j) {
					frequency = Integer.parseInt(items[j]);
					if (frequency != 0) {
						bigramDocsMap.put(key, bigramDocsMap.get(key) + 1);
					}
				}
				System.out.println("已经处理第" + i + "行矩阵！");
			}

			// 写入文件
			IOHelper.writeMapToFile(bigramDocsMap, new File(
					"preprocess/new_develop_test_ngram/bigram_set_freq"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 计算每一篇文档中在词表中的bigram的tfidf值。
	 */
	public static void createWordVocabTFIDF() {
		try {
			List<String> vocab = FileUtils.readLines(new File(
					"preprocess/new_develop_test_ngram/bigram_freq/bigram"));

			// 构建map，确定每一个单词出现的文档数量。
			Map<String, Integer> wordSetNumMap = new HashMap<String, Integer>();
			File file = new File(
					"preprocess/new_develop_test_ngram/bigram_set_freq");
			List<String> wordNumLines = FileUtils.readLines(file);
			String[] two = null;
			for (String line : wordNumLines) {
				two = line.split("\t");
				wordSetNumMap.put(two[0], Integer.parseInt(two[1]));
			}

			File dir = new File("preprocess/new_set_doc_bigram_tf");
			int count = 0;
			for (File f : dir.listFiles()) {
				count++;
				Map<String, Double> tfidf = new HashMap<String, Double>();
				List<String> lines = FileUtils.readLines(f);
				String[] twoItems = null;
				String word = null;
				double tf = 0.0;
				double idf = 0.0;
				for (String line : lines) {
					twoItems = line.split("\t");
					word = twoItems[0];
					tf = Double.parseDouble(twoItems[1]);
					if (vocab.contains(word)) {

						idf = Math.log(Constants.docSetCount * 1.0
								/ (wordSetNumMap.get(word) + 1))
								/ Math.log(2);
						tfidf.put(word, tf * idf);
					}
				}
				IOHelper.writeMapToFile(tfidf, new File(
						"preprocess/new_set_doc_bigram_tfidf/" + f.getName()));
				System.out.println("处理第" + count + "篇文档完成！");
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
